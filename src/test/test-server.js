/**
 * Teste automatizado de login
 */

// Importando bibliotecas
const assert = require('chai').assert;
const net = require('net');
const TetrinetClient = require('../../server-2019-2/client/src/TetrinetClient/TetrinetClient').TetrinetClient;


// Criando conexão com o cliente
const tetrinetClient = new TetrinetClient();
tetrinetClient.connect({ username: "FernandoSev" });

answertetrinetClient = '';
tetrinetClient.onReceiveData((message) => {
  answertetrinetClient = message.toString();
  tetrinetClient.closeConnection();
});

// Testes Server
describe('Testando a função: Login', () => {
  it('Deve retonar uma string contendo: "FernandoSev"', () => {
    assert.include('FernandoSev', answertetrinetClient, 'Erro');
  });
});